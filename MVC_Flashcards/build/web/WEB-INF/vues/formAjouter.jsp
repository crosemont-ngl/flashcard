<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
    <head>
        <meta charset="utf-8">
        <title>Cr�ation de la carte</title>
        <style><jsp:include page="../css/styleFormFiche.css"/></style>
    </head>
    <body>
        <div class="wrapper">
                <div class="title">
                    Cr�ation
                </div>
            <form action="${pageContext.request.contextPath}/ControlleurFrontal?accion=ajouterMatiere"
                  method="POST" class="was-validated">
                <div class="form">
                    <div class="input_field">
                        <label>Nom mati�re</label>
                        <input  type="text" name="nomMatiere" class="input" required>
                    </div>
                    <div class="input_field">
                        <input type="submit" value="Cr�er une mati�re" class="btn">
                    </div>
                </div>
            </form>
            <form action="${pageContext.request.contextPath}/ControlleurFrontal?accion=insererFiche"
                  method="POST" class="was-validated">
                    <div class="form">
                        <div class="input_field">
                            <label>Mati�re</label>
                            <div class="custom_select">
                                <select name="matiere" required>
                                    <option value="">S�lectionner</option>
                                    <c:forEach var="matiere" items="${matieres}" varStatus="status">
                                        <option value="${matiere.idMatiere}">${matiere.nomMatiere}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </div>
                        <div class="input_field">
                            <label>Question</label>
                            <textarea name="question" class="textarea" required></textarea>
                        </div>
                        <div class="input_field">
                            <label>R�ponse</label>
                            <input type="text" name="reponse" class="input" required>
                        </div>
                        <div class="input_field">
                            <input type="submit" value="Cr�er une fiche" class="btn">
                        </div>

                    </div>
            </form>
        </div>
    </body>
</html>
